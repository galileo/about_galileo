$( document ).ready(function() {
    console.log( "ready!" );
    $('#faqs').DataTable();

    $("#myCarousel").on("slide.bs.carousel", function(e) {
        var $e = $(e.relatedTarget);
        var idx = $e.index();
        var itemsPerSlide = 3;
        var totalItems = $("#myCarousel .carousel-item").length;
    
        if (idx >= totalItems - (itemsPerSlide - 1)) {
        var it = itemsPerSlide - (totalItems - idx);
    
        for (var i = 0; i < it; i++) {   
            // append slides to end
            if (e.direction == "left") {
            $("#myCarousel .carousel-item")
                .eq(i)
                .appendTo("#myCarousel .carousel-inner");
            } else {
            $(".carousel-item")
                .eq(0)
                .appendTo($(this).find(".carousel-inner"));
            }
            
        }
        }
    });
    
    /* deep linking tabs */
    var url = window.location.href;     
    if (url.indexOf("#") > 0){     
        var activeTab = url.substring(url.indexOf("#") + 1);         
        $('.nav[role="tablist"] a[href="#'+activeTab+'"]').tab('show');     
    }

});