import { CountUp } from './countUp.min.js';

const options = {
    duration: 8,
  };

window.onload = function() {
  var total1 = new CountUp('total-libraries', 2836, options);
  total1.start();
  var total2 = new CountUp('total-queries', 48, options);
  total2.start();
  var total3 = new CountUp('total-saved', 124, options);
  total3.start();
}