// The require statement tells Node to look into the node_modules folder for a package
// Once the package is found, we assign its contents to the variable
// gulp.src tells the Gulp task what files to use for the task
// gulp.dest tells Gulp where to output the files once the task is completed.
var gulp = require('gulp'),
    browserSync = require('browser-sync').create(),
    sass = require('gulp-sass'),
    clean = require('gulp-clean'),
    panini = require('panini'),
    sourcemaps = require('gulp-sourcemaps'),
    imagemin = require('gulp-imagemin'),
    cache = require('gulp-cache'),
    minify = require('gulp-minify'),
    postcss = require('gulp-postcss'),
    cssnano = require('cssnano');
    autoprefixer = require('gulp-autoprefixer');
    merge = require('merge-stream'),
    rename = require('gulp-rename'),

// Copy third party libraries from node_modules into /vendor
gulp.task("vendor:js", function() {
    return gulp
      .src([
        "./node_modules/bootstrap/dist/js/*",
        "./node_modules/jquery/dist/*",
        "!./node_modules/jquery/dist/core.js",
        "./node_modules/popper.js/dist/umd/popper.*",
        "./node_modules/datatables.net/js/jquery.dataTables.min.js",
        "./node_modules/datatables.net-bs4/js/dataTables.bootstrap4.min.js",
        "./node_modules/countup.js/dist/countUp.min.js"
      ])
      .pipe(gulp.dest("./src/assets/js/vendor", { allowEmpty: true }));
  });
  
  // Copy font-awesome from node_modules into /fonts
  gulp.task("vendor:fonts", function() {
    return gulp
      .src([
        "./node_modules/@fortawesome/fontawesome-free/**/*",
        "!./node_modules/@fortawesome/fontawesome-free/{less,less/*}",
        "!./node_modules/@fortawesome/fontawesome-free/{scss,scss/*}",
        "!./node_modules/@fortawesome/fontawesome-free/{sprites,sprites/*}",
        "!./node_modules/@fortawesome/fontawesome-free/{svgs,svgs/*}",
        "!./node_modules/@fortawesome/fontawesome-free/.*",
        "!./node_modules/@fortawesome/fontawesome-free/*.{txt,json,md}"
      ])
      .pipe(gulp.dest("./src/assets/fonts/font-awesome", { allowEmpty: true }));
  });
  
  // vendor task
  gulp.task("vendor", gulp.parallel("vendor:fonts", "vendor:js"));
  
  // Copy vendor's js to /dist
  gulp.task("vendor:build", function() {
    var jsStream = gulp
      .src([
        "./src/assets/js/vendor/bootstrap.bundle.min.js",
        "./src/assets/js/vendor/jquery.slim.min.js",
        "./src/assets/js/vendor/popper.min.js",
        "./src/assets/js/vendor/jquery.dataTables.min.js",
        "./src/assets/js/vendor/dataTables.bootstrap4.min.js",
        "./node_modules/countup.js/dist/countUp.min.js",
        "./src/assets/js/stats.js"
      ])
      .pipe(gulp.dest("./dist/assets/js/vendor"));
    var fontStream = gulp
      .src(["./src/assets/fonts/font-awesome/**/*.*"])
      .pipe(gulp.dest("./dist/assets/fonts/font-awesome", { allowEmpty: true }));
    return merge(jsStream, fontStream);
  });

  // Copy DataTables CSS from node_modules to /assets/css/vendors
  gulp.task("dataTables:css", function() {
    return gulp
      .src(["./node_modules/datatables.net-bs4/css/dataTables.bootstrap4.min.css"])
      .pipe(gulp.dest("./dist/assets/css", { allowEmpty: true }));
  });
  
  // Copy Bootstrap SCSS(SASS) from node_modules to /assets/scss/bootstrap
  gulp.task("bootstrap:scss", function() {
    return gulp
      .src(["./node_modules/bootstrap/scss/**/*"])
      .pipe(gulp.dest("./src/assets/scss/bootstrap", { allowEmpty: true }));
  });
  
  // Compile SCSS(SASS) files
  gulp.task(
    "scss",
    gulp.series("bootstrap:scss", function compileScss() {
      return gulp
        .src(["./src/assets/scss/**/*.scss"])
        .pipe(
          sass
            .sync({
              outputStyle: "expanded"
            })
            .on("error", sass.logError)
        )
        .pipe(autoprefixer())
        .pipe(gulp.dest("./src/assets/css", { allowEmpty: true }));
    })
  );

// ------------ Development Tasks -------------
// Compile Sass into CSS
gulp.task('sass', function () {
    var plugins = [
        cssnano()
      ];
    return gulp.src(['src/assets/scss/*.scss'])
        .pipe(sourcemaps.init())
        .pipe(sass({ 
            outputStyle: 'expanded',
            sourceComments: 'map',
            sourceMap: 'sass',
            outputStyle: 'nested'
        }).on('error', sass.logError))
        .pipe(autoprefixer('last 2 versions'))
        .pipe(rename({suffix: '.min'}))
        .pipe(postcss(plugins))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest("dist/assets/css"))
        .pipe(browserSync.stream());
});

// Using panini, template, page and partial files are combined to form html markup
gulp.task('compile-html', function () {
    return gulp.src('src/pages/**/*.html')
        .pipe(panini({
            root: 'src/pages/',
            layouts: 'src/layouts/',
            partials: 'src/partials/',
            helpers: 'src/helpers/',
            data: 'src/data/'
        }))
        .pipe(gulp.dest('dist', { allowEmpty: true }));
});

// Reset Panini's cache of layouts and partials
gulp.task('resetPages', gulp.series(function(done) { 
    panini.refresh();
    done();
    console.log('Clearing panini cache');
}));

// Watches for changes while gulp is running
gulp.task('watch', gulp.series('sass', function() { 
    // Live reload with BrowserSync
    browserSync.init({
        server: "./dist"
    });
    gulp.watch('src/assets/js/**/*.js').on('change', gulp.series('scripts', function (done) {
        browserSync.reload();
        done();
    }))
    gulp.watch(['src/assets/scss/**/*','!src/assets/scss/bootstrap/**']).on('change', gulp.series('sass', function (done) {
        browserSync.reload();
        done();
    }))
    gulp.watch('src/assets/img/**/*').on('change', gulp.series('images', function (done) {
        done();
    }))
    gulp.watch('src/**/*.html').on('change', gulp.series('resetPages','compile-html', function (done) {
        browserSync.reload();
        done();
    }))
    console.log('Watching for changes');
}));


// ------------ Optimization Tasks -------------
// Copies image files to dist
gulp.task('images', function () {
    return gulp.src('src/assets/img/**/*.+(png|jpg|jpeg|gif|svg)')
        .pipe(cache(imagemin ([
            imagemin.gifsicle({interlaced: true}),
            imagemin.jpegtran({progressive: true}),
            imagemin.optipng({optimizationLevel: 5}),
            imagemin.svgo({
                plugins: [
                    {removeViewBox: true},
                    {cleanupIDs: false}
                ]
            })
        ]))) // Caching images that ran through imagemin
        .pipe(gulp.dest('dist/assets/img/'));
});

// Concatenating js files
gulp.task('scripts', function () {
    return gulp.src('src/assets/js/app.js')
        .pipe(sourcemaps.init())
        .pipe(sourcemaps.write('./'))
        .pipe(minify())
        .pipe(gulp.dest('dist/assets/js/'))
        .pipe(browserSync.stream());
});

// Cleaning/deleting files no longer being used in dist folder
    gulp.task('clean:dist', gulp.series(function(done) { 
    console.log('Removing old files from dist');
    return gulp.src('dist', {read: false, allowEmpty: true})
    .pipe(clean());
    done();
}));


// ------------ Build Sequence -------------
// Simply run 'gulp' in terminal to run local server and watch for changes
gulp.task('default', gulp.series(['clean:dist', 'dataTables:css', 'scripts', 'images', 'compile-html', 'resetPages', 'vendor', 'vendor:build', 'watch']));

// Creates production ready assets in dist folder
gulp.task('build', gulp.series('clean:dist', gulp.parallel('dataTables:css', 'sass', ['scripts', 'images', 'compile-html','vendor']), 'vendor:build'))

